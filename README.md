
## trisect_calibration_ros.py

A specialized version of the [run_calibration_ros.py](https://gitlab.com/apl-ocean-engineering/camera_calibration/charuco_stereo_calibration/-/blob/main/scripts/run_calibration_ros.py) script from [charuco_stereo_calibration](https://gitlab.com/apl-ocean-engineering/camera_calibration/charuco_stereo_calibration) tuned for Trisect calibration.  Typically this is done by creating a ROS package which contains the relevant bag and a launchfile based on the [sample launchfile in this repo](https://gitlab.com/apl-ocean-engineering/camera_calibration/charuco_stereo_calibration/-/blob/main/launch/calibrate_trisect.launch).

As an example, see [2022-09-21_trisect1_in_air_calibration](https://gitlab.com/rsa-perception-sensor/trisect_analysis/-/tree/master/2022-09-21_trisect1_in_air_calibration) in [trisect_analysis](https://gitlab.com/rsa-perception-sensor/trisect_analysis/).
